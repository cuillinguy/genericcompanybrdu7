new MutationObserver(function (mutations) {
    mutations.some(function (mutation) {
        if (mutation.type === 'attributes' && mutation.attributeName === 'src') {
            console.log(mutation);
            console.log('Old src: ', mutation.oldValue);
            console.log('New src: ', mutation.target.src);
            return true;
        }

        return false;
    });
}).observe(document.body, {
    attributes: true,
    attributeFilter: ['src'],
    attributeOldValue: true,
    characterData: false,
    characterDataOldValue: false,
    childList: false,
    subtree: true
});

setTimeout(function () {
    document.getElementsByTagName('p_lt_ctl04_pageplaceholder_p_lt_ctl00_SagePayPaymentForm_PaymentDataForm_iframeSagePayment')[0].src = 'http://jsfiddle.net/';
}, 3000);
                