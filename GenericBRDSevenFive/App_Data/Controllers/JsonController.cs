﻿using GenericBusinessFactory;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.WebApi;
using static GenericBusinessFactory.ReturnObjects;

namespace GenericBRDSevenFive.App_Data.Controllers
{
    public class JsonController : UmbracoApiController
    {

        UmbracoHelper helper { get; set; }

        public JsonController() {

            helper = new UmbracoHelper(UmbracoContext.Current);
        }

        #region Home Methods
        

        [System.Web.Http.HttpGet]
        public IHttpActionResult Home()
        {
            //var helper = new UmbracoHelper(UmbracoContext.Current);
            var appSettings = ConfigurationManager.AppSettings;
            Guid homeGuid = new Guid(appSettings["homeNodeGuid"]);
            var response = new JsonHomeResponse();

            try
            {   
                //Faster Method --retrieved from the Umbraco.config xml cache rather than the database
                var typedHomeRootNodeId = helper.TypedContentAtRoot().FirstOrDefault().Id;

                var homeByGuid = helper.TypedContent(typedHomeRootNodeId);

                var homeObject = (CMSWebsite.Home)homeByGuid;

                if (homeObject != null)
                {
                    response.h1 = homeObject.H1;
                    response.h2 = homeObject.H2;
                }


            }
            catch (Exception)
            {

                throw;
            }

            return Json(response);

        }

        #endregion

        #region Blogs

        [System.Web.Http.HttpGet]
        public IHttpActionResult Blog(string nodeId)
        {
            var appSettings = ConfigurationManager.AppSettings;

            var helper = new UmbracoHelper(UmbracoContext.Current);
            var response = new BlogJsonResponse();
            var blogById = helper.TypedContent(Convert.ToInt32(nodeId));
            var blog = (CMSWebsite.BlogItem)blogById;

            try
            {
                response.data.content.title = blog.Title;
                response.data.content.dateCreated = blog.CreateDate.ToString("s");
                response.data.content.id = nodeId;
                response.data.content.blogs = GetAllOtherBlogsOtherThanThisOne(nodeId);

                //get the sections
                var blogBodySections = blogById.Children;
                IEnumerable<IPublishedContent> sortedBodySection = blogBodySections.Take(10);

                foreach (CMSWebsite.BlogSection item in sortedBodySection)
                {
                    var blogBodySection = item.BlogContentSection.ToString();
                    response.data.content.blogbodysections.Add(blogBodySection);
                }

                //loop through the section image

                foreach (CMSWebsite.BlogSectionImage item in blog.ImageSections)
                {

                    var sectionImageResponse = new BlogSectionImage() {

                         description = item.ImageDescription.ToString(),
                          imageTitle = item.ImageTitle.ToString(),
                           imageUrl = item.Url

                    };

                    response.data.content.blogSectionImages.Add(sectionImageResponse);
                }

            }
            catch (Exception)
            {

                throw;
            }


            return Json(response);
        }

        /// <summary>
        /// Get Blog (id)
        /// Gets all other blogs apart from the selected one
        /// </summary>
        /// <param name="selectedBlogId"></param>
        /// <returns></returns>
        public List<IBlogCollectionItem> GetAllOtherBlogsOtherThanThisOne(string selectedBlogId) {

            var result = new List<IBlogCollectionItem>();

            var helper = new UmbracoHelper(UmbracoContext.Current);

            var appSettings = ConfigurationManager.AppSettings;
            var blogsNodeId = GetBlogsNodeId();

            try
            {

                IPublishedContent currentNode = Umbraco.TypedContent(blogsNodeId);//Root blog node

                //get title of current Blog
                var blogById = helper.TypedContent(Convert.ToInt32(selectedBlogId));
                var currentBlog = (CMSWebsite.BlogItem)blogById;

                //Get all other blogs other than this one

                var pages = currentNode.Children;
                IEnumerable<IPublishedContent> sortedBlogs = pages.OrderByDescending(x => x.CreateDate).Take(10);

                foreach (CMSWebsite.BlogItem item in sortedBlogs)
                {
                    if (item.Name != currentBlog.Name)
                    {
                        IBlogCollectionItem blog = new BlogCollectionItem
                        {
                            id = item.Id.ToString(),
                            image = GetDomainRoot() + item.FeaturedImage.Url(),
                            title = item.Name,
                            dateCreated = item.CreateDate.ToString("s")
                        };
                        result.Add(blog);
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }

            return result;

        }




        #endregion

        #region Companies
        #endregion

        #region Download Files
        #endregion

        #region Helper Methods

        /// <summary>
        /// Gets the domain root of this instance of the website
        /// TODO: Probably this should come from another source if we are using cloned projects
        /// </summary>
        /// <returns></returns>
        public string GetDomainRoot()
        {

            var appSettings = ConfigurationManager.AppSettings;
            var domainRoot = appSettings["RootDomain"].ToString();

            return domainRoot;
        }



        /// <summary>
        /// Get the blogs NodeId
        /// From Settings
        /// </summary>
        /// <returns></returns>
        public int GetBlogsNodeId()
        {
            var helper = new UmbracoHelper(UmbracoContext.Current);
            var appSettings = ConfigurationManager.AppSettings;
            Guid settingsGuid = new Guid(appSettings["SettingsNodeGuid"]);
            var settingsByGuid = helper.TypedContent(settingsGuid);
            var settingsObject = (CMSWebsite.Settings)settingsByGuid;

            try
            {
                var blogNodeId = settingsObject.BlogFolderNode.FirstOrDefault().Id;

                return blogNodeId;

            }
            catch (Exception)
            {

                throw;
            }
        }


        /// <summary>
        /// Gets the News Root FolderId
        /// </summary>
        /// <returns></returns>
        public int GetNewsNodeId()
        {
            var helper = new UmbracoHelper(UmbracoContext.Current);
            var appSettings = ConfigurationManager.AppSettings;
            Guid settingsGuid = new Guid(appSettings["SettingsNodeGuid"]);
            var settingsByGuid = helper.TypedContent(settingsGuid);
            var settingsObject = (CMSWebsite.Settings)settingsByGuid;

            try
            {

                var newsNodeId = settingsObject.NewsFolderNode.FirstOrDefault().Id;

                return newsNodeId;

            }
            catch (Exception)
            {

                throw;
            }


        }

        /// <summary>
        /// Gets the downloads page node
        /// </summary>
        /// <returns></returns>
        public int GetDownloadsPageNodeId()
        {

            var helper = new UmbracoHelper(UmbracoContext.Current);
            var appSettings = ConfigurationManager.AppSettings;
            Guid settingsGuid = new Guid(appSettings["SettingsNodeGuid"]);
            var settingsByGuid = helper.TypedContent(settingsGuid);
            var settingsObject = (CMSWebsite.Settings)settingsByGuid;

            try
            {
                var downloadsPageNode = settingsObject.DownloadFilesNode.FirstOrDefault().Id;

                return downloadsPageNode;

            }
            catch (Exception)
            {

                throw;
            }
        }


        /// <summary>
        /// Get the Company Details NodeId
        /// </summary>
        /// <returns></returns>
        public int GetCompanyDetailsNodeId()
        {

            var helper = new UmbracoHelper(UmbracoContext.Current);
            var appSettings = ConfigurationManager.AppSettings;
            Guid settingsGuid = new Guid(appSettings["SettingsNodeGuid"]);
            var settingsByGuid = helper.TypedContent(settingsGuid);
            var settingsObject = (CMSWebsite.Settings)settingsByGuid;

            try
            {
                var companyDetailsNode = settingsObject.CompanyDetailsNode.FirstOrDefault().Id;

                return companyDetailsNode;

            }
            catch (Exception)
            {

                throw;
            }
        }


        #endregion

        #region Shared Methods

        //public List<ICarouselImage> GetCarouselImages() {

        //    try
        //    {

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }


        //}

        #endregion




    }


}