﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Models;

namespace GenericBRDSevenFive.App_Data.Controllers
{
    public class HomeController : Umbraco.Web.Mvc.RenderMvcController
    {
        // GET: Home
        public ActionResult Blog(RenderModel model)
        {
            return View(model);
        }
    }
}