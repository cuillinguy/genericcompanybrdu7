﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GenericBusinessFactory.ReturnObjects;

namespace GenericBusinessFactory
{
    public interface IBlogItem
    {
        string id { get; set; }
        string heroImage { get; set; }
        string title { get; set; }
        string dateCreated { get; set; }
        string blockQuote { get; set; }
        List<string> blogbodysections { get; set; }
        List<IBlogSectionImage> blogSectionImages { get; set; }
        string bodyOne { get; set; }
        string bodyTwo { get; set; }
        string bodyThree { get; set; }
        string blogsTitle { get; set; }
        List<IBlogCollectionItem> blogs { get; set; }

        List<IBlogSection> sections { get; set; }
    }
}
