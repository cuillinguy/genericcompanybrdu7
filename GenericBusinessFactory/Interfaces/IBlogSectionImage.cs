﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public interface IBlogSectionImage
    {
        string imageTitle { get; set; }
        string imageUrl { get; set; }
        string size { get; set; }
        string type { get; set; }
        string description { get; set; }
    }
}
