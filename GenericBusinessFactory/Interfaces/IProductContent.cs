﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public interface IProductContent
    {
        string h1 { get; set; }
        string h2 { get; set; }
        string productName { get; set; }
        string description { get; set; }
        string price { get; set; }

        List<IProductImage> images { get; set; }
    }
}
