﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public interface IJsonHomeResponse 
    {
        string h1 { get; set; }
        string h2 { get; set; }
        List<ICarouselImage> carouselImages { get; set; }
    }
}
