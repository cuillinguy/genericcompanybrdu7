﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public interface IBlogCollectionItem
    {
        string id { get; set; }
        string image { get; set; }
        string title { get; set; }
        string dateCreated { get; set; }
    }
}
