﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GenericBusinessFactory.ReturnObjects;

namespace GenericBusinessFactory
{
    public interface IContactContent
    {
        string h1 { get; set; }
        string h2 { get; set; }
        string companyName { get; set; }
        string mainPhone { get; set; }
        string mainEmail { get; set; }
        string formTitle { get; set; }
        string buttonText { get; set; }
        string facebookUrl { get; set; }
        string linkedinUrl { get; set; }
        string twitterUrl { get; set; }
        List<ICompanyLocation> companyLocations { get; set; }
    }
}
