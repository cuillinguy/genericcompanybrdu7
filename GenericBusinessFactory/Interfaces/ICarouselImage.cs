﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public interface ICarouselImage
    {
        string id { get; set; }
        string image { get; set; }
        string flash { get; set; }
        string link { get; set; }
        string buttonText { get; set; }
        string title { get; set; }
    }
}
