﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public interface ICompanyLocation
    {
        string title { get; set; }
        string subTitle { get; set; }
        string address { get; set; }
        string city { get; set; }
        string county { get; set; }
        string postCode { get; set; }
        string longitude { get; set; }
        string latitude { get; set; }
    }
}
