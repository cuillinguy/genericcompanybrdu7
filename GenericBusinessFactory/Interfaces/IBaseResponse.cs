﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public interface IBaseResponse
    {
        MainMenu mainMenu { get; set; }
    }
}
