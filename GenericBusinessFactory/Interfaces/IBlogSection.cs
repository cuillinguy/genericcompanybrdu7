﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public interface IBlogSection
    {
        string image { get; set; }
        string text { get; set; }
    }
}
