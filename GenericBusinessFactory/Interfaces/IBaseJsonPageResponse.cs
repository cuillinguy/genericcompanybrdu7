﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public interface IBaseJsonPageResponse
    {
        string id { get; set; }
        string url { get; set; }
        string pageType { get; set; }
    }
}
