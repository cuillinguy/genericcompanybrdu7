﻿using GenericBusinessFactory.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public class JsonHomeResponse : BaseResponse,  IJsonHomeResponse 
    {
        public string h1 { get; set; }
        public string h2 { get; set; }
        public List<ICarouselImage> carouselImages { get; set; }
        //public MainMenu mainMenu { get; set; }

        public JsonHomeResponse() {
            carouselImages = new List<ICarouselImage>();
        }
    }
}
