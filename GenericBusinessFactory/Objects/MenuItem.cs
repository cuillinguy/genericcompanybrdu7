﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public class MenuItem : IMenuItem
    {
        public MenuItem()
        {
        }

        public string id { get; set; }
        public string url { get; set; }
        public string pageType { get; set; }
        public string thumbNail { get; set; }


    }
}
