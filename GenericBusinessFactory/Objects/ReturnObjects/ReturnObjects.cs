﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public class ReturnObjects
    {
        #region Blog Classes

        public class BlogJsonResponse
        {

            public BlogData data { get; set; }

            public BlogJsonResponse()
            {

                data = new BlogData();
            }

        }

        /// <summary>
        /// Individuial blog item 
        /// when called from the Blog Method
        /// </summary>
        public class BlogItem : IBlogItem
        {

            public string id { get; set; }
            public string heroImage { get; set; }
            public string title { get; set; }
            public string dateCreated { get; set; }
            public string blockQuote { get; set; }
            public List<string> blogbodysections { get; set; }
            public List<IBlogSectionImage> blogSectionImages { get; set; }
            public string bodyOne { get; set; }
            public string bodyTwo { get; set; }
            public string bodyThree { get; set; }
            public string blogsTitle { get; set; }
            public List<IBlogCollectionItem> blogs { get; set; }

            public List<IBlogSection> sections { get; set; }

            public BlogItem()
            {
                sections = new List<IBlogSection>();
                blogs = new List<IBlogCollectionItem>();
                blogbodysections = new List<string>();
                blogSectionImages = new List<IBlogSectionImage>();
            }
        }

        public class BlogSection : IBlogSection
        {

            public string image { get; set; }
            public string text { get; set; }

        }



        /// <summary>
        /// Class for the blogs collection in
        /// the Blog detail page
        /// </summary>
        public class BlogCollectionItem : IBlogCollectionItem
        {

            public string id { get; set; }
            public string image { get; set; }
            public string title { get; set; }
            public string dateCreated { get; set; }

        }

        public class BlogSectionImage : IBlogSectionImage
        {
            public string imageTitle { get ; set; }
            public string imageUrl { get; set; }
            public string size { get; set; }
            public string type { get; set; }
            public string description { get; set; }
        }

        #endregion

        #region About the company classes

        public class ContactResponse : IContactResponse
        {

            public ContactData data { get; set; }

            public ContactResponse()
            {

                data = new ContactData();
            }
        }

        public class ContactContent : IContactContent
        {

            public string title { get; set; }
            public string subtitle { get; set; }
            public string companyName { get; set; }
            public string phone { get; set; }
            public string email { get; set; }
            public string formTitle { get; set; }
            public string formButton { get; set; }
            public string facebookUrl { get; set; }
            public string linkedinUrl { get; set; }
            public string twitterUrl { get; set; }
            public string h1 { get; set; }
            public string h2 { get; set; }
            public string mainPhone { get; set; }
            public string mainEmail { get; set; }
            public string buttonText { get; set; }
            public List<ICompanyLocation> companyLocations { get; set; }

            public ContactContent()
            {
                companyLocations = new List<ICompanyLocation>();
            }


        }

        public class CompanyLocation : ICompanyLocation
        {

            public string title { get; set; }
            public string subTitle { get; set; }
            public string address { get; set; }
            public string city { get; set; }
            public string county { get; set; }
            public string postCode { get; set; }
            public string longitude { get; set; }
            public string latitude { get; set; }


        }


        #endregion

        #region Product Classes

        public class ProductJsonResponse
        {
            public ProductData data { get; set; }

            public ProductJsonResponse() {

                data = new ProductData();
            }
        }

        public class ProductContent : IProductContent 
        {
           public string h1 { get; set; }
           public string h2 { get; set; }
           public string productName { get; set; }
           public string description { get; set; }
           public string price { get; set; }
           
           public List<IProductImage> images { get; set; }

            public ProductContent() {

                images = new List<IProductImage>();
            }
        }

        #endregion



        #region Downloads classes

        public class DownloadsJsonResponse {

            public DownloadsData data { get; set; }

            public DownloadsJsonResponse() {

                data = new DownloadsData();
            }

        }

        public class DownloadsPageItem {

            public string h1 { get; set; }

            public List<DownloadFile> downloadFiles { get; set; }

            public DownloadsPageItem() {

                downloadFiles = new List<DownloadFile>();
            }

        }

        #endregion

        #region TestResponse

        public class TestJsonResponse {

            public TestContent content { get; set; }

            public TestJsonResponse() {

                content = new TestContent();

            }

        }

        #endregion

        #region Shared Objects

        public class CarouselImage : ICarouselImage
        {
            public string id { get; set; }
            public string image { get; set; }
            public string flash { get; set; }
            public string link { get; set; }
            public string buttonText { get; set; }
            public string title { get; set; }

        }

        #endregion

    }
}
