﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GenericBusinessFactory.ReturnObjects;

namespace GenericBusinessFactory
{
    public class BlogData
    {
        public IBlogItem content { get; set; }


        public BlogData()
        {
            content = new BlogItem();

        }
    }


    public class TestContent {

        public TestItem testContent { get; set; }

        public TestContent() {
            testContent = new TestItem();
        }
    }


    public class DownloadsData {

        public DownloadsPageItem content { get; set; }

        public DownloadsData() {

            content = new DownloadsPageItem();
        }

    }



}
