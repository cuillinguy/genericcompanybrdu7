﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GenericBusinessFactory.ReturnObjects;

namespace GenericBusinessFactory
{
    public class ContactData : IContactData
    {
        public ContactContent content { get; set; }

        public ContactData() {

            content = new ContactContent();
        }
    }
}
