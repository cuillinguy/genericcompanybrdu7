﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GenericBusinessFactory.ReturnObjects;

namespace GenericBusinessFactory
{
    public class ProductData : IProductData
    {
        public ProductContent content { get; set; }

        public ProductData() {

            content = new ProductContent();
        }
    }
}
