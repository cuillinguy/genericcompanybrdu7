﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public class ProductImage : IProductImage
    {
        public string image { get; set; }
        public string description { get; set; }
    }
}
