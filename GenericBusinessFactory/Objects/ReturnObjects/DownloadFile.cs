﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public class DownloadFile
    {
        public string filetype { get; set; }
        public string size { get; set; }
        public string description { get; set; }
        public string thumbnailUrl { get; set; }
        public string filelink { get; set; }
    }
}
