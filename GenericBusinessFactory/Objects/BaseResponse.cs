﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory.Objects
{
    public class BaseResponse : IBaseResponse
    {
        public MainMenu mainMenu { get; set; }
    }
}
