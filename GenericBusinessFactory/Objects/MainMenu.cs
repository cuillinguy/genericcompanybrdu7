﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBusinessFactory
{
    public class MainMenu : IMainMenu
    {
        public MainMenu()
        {
            menuItems = new List<MenuItem>();
        }

        public List<MenuItem> menuItems { get; set; }


    }
}
